@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.inputData.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.input-datas.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="nama_input_proses_data">{{ trans('cruds.inputData.fields.nama_input_proses_data') }}</label>
                <input class="form-control {{ $errors->has('nama_input_proses_data') ? 'is-invalid' : '' }}" type="text" name="nama_input_proses_data" id="nama_input_proses_data" value="{{ old('nama_input_proses_data', '') }}" required>
                @if($errors->has('nama_input_proses_data'))
                <div class="invalid-feedback">
                    {{ $errors->first('nama_input_proses_data') }}
                </div>
                @endif
                <span class="help-block">{{ trans('cruds.inputData.fields.nama_input_proses_data_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="data_satus">{{ trans('cruds.inputData.fields.data_satu') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control" name="data_satus[]" id="data_satus">
                    <option value="" selected disabled>pilih data satu</option>
                    @foreach($data_satus as $id => $data_satu)
                    <option value="{{ $id }}">{{ $data_satu }}</option>
                    @endforeach
                </select>
                @if($errors->has('data_satus'))
                <div class="invalid-feedback">
                    {{ $errors->first('data_satus') }}
                </div>
                @endif
                <span class="help-block">{{ trans('cruds.inputData.fields.data_satu_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="data_duas">{{ trans('cruds.inputData.fields.data_dua') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control" name="data_duas[]" id="data_duas"></select>
                @if($errors->has('data_duas'))
                <div class="invalid-feedback">
                    {{ $errors->first('data_duas') }}
                </div>
                @endif
                <span class="help-block">{{ trans('cruds.inputData.fields.data_dua_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="data_tigas">{{ trans('cruds.inputData.fields.data_tiga') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control" name="data_tigas[]" id="data_tigas"></select>
                @if($errors->has('data_tigas'))
                <div class="invalid-feedback">
                    {{ $errors->first('data_tigas') }}
                </div>
                @endif
                <span class="help-block">{{ trans('cruds.inputData.fields.data_tiga_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="data_empats">{{ trans('cruds.inputData.fields.data_empat') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control" name="data_empats[]" id="data_empats"></select>
                @if($errors->has('data_empats'))
                <div class="invalid-feedback">
                    {{ $errors->first('data_empats') }}
                </div>
                @endif
                <span class="help-block">{{ trans('cruds.inputData.fields.data_empat_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>
<script>
    // when data satus dropdown changes
    $('#data_satus').change(function() {
        var satusID = $(this).val();
        if (satusID) {
            $.ajax({
                type: "GET",
                url: "{{ url('getDua') }}?id_satus=" + satusID,
                success: function(res) {

                    if (res) {

                        $("#data_duas").empty();
                        $("#data_duas").append('<option>pilih data dua</option>');
                        $.each(res, function(key, value) {
                            $("#data_duas").append('<option value="' + value + '">' + key +
                                '</option>');
                        });

                    } else {
                        $("#data_duas").empty();
                    }
                }
            });
        } else {
            $("#data_duas").empty();
            $("#data_satus").empty();
        }
    });

    // when data duas dropdown changes
    $('#data_duas').on('change', function() {
        var duasID = $(this).val();
        if (duasID) {
            $.ajax({
                type: "GET",
                url: "{{ url('getTiga') }}?id_duas=" + duasID,
                success: function(res) {

                    if (res) {

                        $("#data_tigas").empty();
                        $("#data_tigas").append('<option>pilih data tiga</option>');
                        $.each(res, function(key, value) {
                            $("#data_tigas").append('<option value="' + value + '">' + key +
                                '</option>');
                        });

                    } else {
                        $("#data_tigas").empty();
                    }
                }
            });
        } else {
            $("#data_tigas").empty();
        }
    });

    // when data tigas dropdown changes
    $('#data_tigas').on('change', function(){
        var tigasID = $(this).val();
        if (tigasID) {
            $.ajax({
                type: "GET",
                url: "{{ url('getEmpat') }}?id_tigas=" + tigasID,
                success: function(res) {
                    if (res) {
                        $("#data_empats").empty();
                        $("#data_empats").append('<option>pilih data empat</option>');
                        $.each(res, function(key, value) {
                            $("#data_empats").append('<option value="' + value + '">' + key +
                                '</option>');
                        });

                    } else {
                        $("#data_empats").empty();
                    }
                }
            });
        } else {
            $("#data_empats").empty();
        }
    });
</script>


@endsection