-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 25 Jul 2022 pada 17.35
-- Versi server: 10.4.14-MariaDB
-- Versi PHP: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uas`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_duas`
--

CREATE TABLE `data_duas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_satus` int(11) NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `team_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data_duas`
--

INSERT INTO `data_duas` (`id`, `id_satus`, `nama`, `created_at`, `updated_at`, `deleted_at`, `team_id`) VALUES
(1, 1, 'pertama kedua', '2022-07-25 05:57:44', '2022-07-25 06:26:54', NULL, NULL),
(2, 2, 'satu dua', '2022-07-25 06:27:14', '2022-07-25 06:27:14', NULL, NULL),
(3, 3, 'dua tiga', '2022-07-25 06:28:35', '2022-07-25 06:28:35', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_dua_input_data`
--

CREATE TABLE `data_dua_input_data` (
  `input_data_id` bigint(20) UNSIGNED NOT NULL,
  `data_dua_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data_dua_input_data`
--

INSERT INTO `data_dua_input_data` (`input_data_id`, `data_dua_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_empats`
--

CREATE TABLE `data_empats` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_tigas` int(11) NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `team_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data_empats`
--

INSERT INTO `data_empats` (`id`, `id_tigas`, `nama`, `created_at`, `updated_at`, `deleted_at`, `team_id`) VALUES
(1, 1, 'dua tiga empat lima', '2022-07-25 05:58:00', '2022-07-25 06:38:09', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_empat_input_data`
--

CREATE TABLE `data_empat_input_data` (
  `input_data_id` bigint(20) UNSIGNED NOT NULL,
  `data_empat_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data_empat_input_data`
--

INSERT INTO `data_empat_input_data` (`input_data_id`, `data_empat_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_satus`
--

CREATE TABLE `data_satus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `team_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data_satus`
--

INSERT INTO `data_satus` (`id`, `nama`, `created_at`, `updated_at`, `deleted_at`, `team_id`) VALUES
(1, 'Pertama', '2022-07-25 05:57:02', '2022-07-25 05:57:27', NULL, NULL),
(2, 'satu', '2022-07-25 05:57:37', '2022-07-25 05:57:37', NULL, NULL),
(3, 'dua', '2022-07-25 06:27:02', '2022-07-25 06:35:40', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_satu_input_data`
--

CREATE TABLE `data_satu_input_data` (
  `input_data_id` bigint(20) UNSIGNED NOT NULL,
  `data_satu_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data_satu_input_data`
--

INSERT INTO `data_satu_input_data` (`input_data_id`, `data_satu_id`) VALUES
(1, 2),
(2, 1),
(3, 1),
(4, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_tigas`
--

CREATE TABLE `data_tigas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_duas` int(11) NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `team_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data_tigas`
--

INSERT INTO `data_tigas` (`id`, `id_duas`, `nama`, `created_at`, `updated_at`, `deleted_at`, `team_id`) VALUES
(1, 1, 'dua tiga empat', '2022-07-25 05:57:53', '2022-07-25 06:35:17', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_tiga_input_data`
--

CREATE TABLE `data_tiga_input_data` (
  `input_data_id` bigint(20) UNSIGNED NOT NULL,
  `data_tiga_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data_tiga_input_data`
--

INSERT INTO `data_tiga_input_data` (`input_data_id`, `data_tiga_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `input_datas`
--

CREATE TABLE `input_datas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_input_proses_data` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `team_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `input_datas`
--

INSERT INTO `input_datas` (`id`, `nama_input_proses_data`, `created_at`, `updated_at`, `deleted_at`, `team_id`) VALUES
(1, 'Proses 1', '2022-07-25 06:45:32', '2022-07-25 06:45:32', NULL, NULL),
(2, 'proses 2', '2022-07-25 08:12:49', '2022-07-25 08:12:49', NULL, NULL),
(3, 'Proses 3', '2022-07-25 08:13:55', '2022-07-25 08:13:55', NULL, NULL),
(4, 'Proses 5', '2022-07-25 08:20:55', '2022-07-25 08:20:55', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(3, '2022_07_11_000001_create_permissions_table', 1),
(4, '2022_07_11_000002_create_roles_table', 1),
(5, '2022_07_11_000003_create_users_table', 1),
(6, '2022_07_11_000004_create_teams_table', 1),
(7, '2022_07_11_000005_create_data_satus_table', 1),
(8, '2022_07_11_000006_create_data_duas_table', 1),
(9, '2022_07_11_000007_create_data_tigas_table', 1),
(10, '2022_07_11_000008_create_data_empats_table', 1),
(11, '2022_07_11_000009_create_input_datas_table', 1),
(12, '2022_07_11_000010_create_permission_role_pivot_table', 1),
(13, '2022_07_11_000011_create_role_user_pivot_table', 1),
(14, '2022_07_11_000012_create_data_satu_input_data_pivot_table', 1),
(15, '2022_07_11_000013_create_data_dua_input_data_pivot_table', 1),
(16, '2022_07_11_000014_create_data_tiga_input_data_pivot_table', 1),
(17, '2022_07_11_000015_create_data_empat_input_data_pivot_table', 1),
(18, '2022_07_11_000016_add_relationship_fields_to_users_table', 1),
(19, '2022_07_11_000017_add_relationship_fields_to_teams_table', 1),
(20, '2022_07_11_000018_add_relationship_fields_to_data_satus_table', 1),
(21, '2022_07_11_000019_add_relationship_fields_to_data_duas_table', 1),
(22, '2022_07_11_000020_add_relationship_fields_to_data_tigas_table', 1),
(23, '2022_07_11_000021_add_relationship_fields_to_data_empats_table', 1),
(24, '2022_07_11_000022_add_relationship_fields_to_input_datas_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `permissions`
--

INSERT INTO `permissions` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'user_management_access', NULL, NULL, NULL),
(2, 'permission_create', NULL, NULL, NULL),
(3, 'permission_edit', NULL, NULL, NULL),
(4, 'permission_show', NULL, NULL, NULL),
(5, 'permission_delete', NULL, NULL, NULL),
(6, 'permission_access', NULL, NULL, NULL),
(7, 'role_create', NULL, NULL, NULL),
(8, 'role_edit', NULL, NULL, NULL),
(9, 'role_show', NULL, NULL, NULL),
(10, 'role_delete', NULL, NULL, NULL),
(11, 'role_access', NULL, NULL, NULL),
(12, 'user_create', NULL, NULL, NULL),
(13, 'user_edit', NULL, NULL, NULL),
(14, 'user_show', NULL, NULL, NULL),
(15, 'user_delete', NULL, NULL, NULL),
(16, 'user_access', NULL, NULL, NULL),
(17, 'team_create', NULL, NULL, NULL),
(18, 'team_edit', NULL, NULL, NULL),
(19, 'team_show', NULL, NULL, NULL),
(20, 'team_delete', NULL, NULL, NULL),
(21, 'team_access', NULL, NULL, NULL),
(22, 'master_data_access', NULL, NULL, NULL),
(23, 'data_satu_create', NULL, NULL, NULL),
(24, 'data_satu_edit', NULL, NULL, NULL),
(25, 'data_satu_show', NULL, NULL, NULL),
(26, 'data_satu_delete', NULL, NULL, NULL),
(27, 'data_satu_access', NULL, NULL, NULL),
(28, 'data_dua_create', NULL, NULL, NULL),
(29, 'data_dua_edit', NULL, NULL, NULL),
(30, 'data_dua_show', NULL, NULL, NULL),
(31, 'data_dua_delete', NULL, NULL, NULL),
(32, 'data_dua_access', NULL, NULL, NULL),
(33, 'data_tiga_create', NULL, NULL, NULL),
(34, 'data_tiga_edit', NULL, NULL, NULL),
(35, 'data_tiga_show', NULL, NULL, NULL),
(36, 'data_tiga_delete', NULL, NULL, NULL),
(37, 'data_tiga_access', NULL, NULL, NULL),
(38, 'data_empat_create', NULL, NULL, NULL),
(39, 'data_empat_edit', NULL, NULL, NULL),
(40, 'data_empat_show', NULL, NULL, NULL),
(41, 'data_empat_delete', NULL, NULL, NULL),
(42, 'data_empat_access', NULL, NULL, NULL),
(43, 'proses_data_access', NULL, NULL, NULL),
(44, 'input_data_create', NULL, NULL, NULL),
(45, 'input_data_edit', NULL, NULL, NULL),
(46, 'input_data_show', NULL, NULL, NULL),
(47, 'input_data_delete', NULL, NULL, NULL),
(48, 'input_data_access', NULL, NULL, NULL),
(49, 'profile_password_edit', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `permission_role`
--

CREATE TABLE `permission_role` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `permission_role`
--

INSERT INTO `permission_role` (`role_id`, `permission_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 26),
(1, 27),
(1, 28),
(1, 29),
(1, 30),
(1, 31),
(1, 32),
(1, 33),
(1, 34),
(1, 35),
(1, 36),
(1, 37),
(1, 38),
(1, 39),
(1, 40),
(1, 41),
(1, 42),
(1, 43),
(1, 44),
(1, 45),
(1, 46),
(1, 47),
(1, 48),
(1, 49),
(2, 22),
(2, 23),
(2, 24),
(2, 25),
(2, 26),
(2, 27),
(2, 28),
(2, 29),
(2, 30),
(2, 31),
(2, 32),
(2, 33),
(2, 34),
(2, 35),
(2, 36),
(2, 37),
(2, 38),
(2, 39),
(2, 40),
(2, 41),
(2, 42),
(2, 43),
(2, 44),
(2, 45),
(2, 46),
(2, 47),
(2, 48),
(2, 49);

-- --------------------------------------------------------

--
-- Struktur dari tabel `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', NULL, NULL, NULL),
(2, 'User', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_user`
--

CREATE TABLE `role_user` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `teams`
--

CREATE TABLE `teams` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `owner_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` datetime DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `team_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`, `team_id`) VALUES
(1, 'Admin', 'admin@admin.com', NULL, '$2y$10$la67lA2ocOKqoMVY0Jni6OeS5rJJvnkWkI2kTEfkTeTol/y8Q6nmC', NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `data_duas`
--
ALTER TABLE `data_duas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `team_fk_6963773` (`team_id`);

--
-- Indeks untuk tabel `data_dua_input_data`
--
ALTER TABLE `data_dua_input_data`
  ADD KEY `input_data_id_fk_6963799` (`input_data_id`),
  ADD KEY `data_dua_id_fk_6963799` (`data_dua_id`);

--
-- Indeks untuk tabel `data_empats`
--
ALTER TABLE `data_empats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `team_fk_6963785` (`team_id`);

--
-- Indeks untuk tabel `data_empat_input_data`
--
ALTER TABLE `data_empat_input_data`
  ADD KEY `input_data_id_fk_6963801` (`input_data_id`),
  ADD KEY `data_empat_id_fk_6963801` (`data_empat_id`);

--
-- Indeks untuk tabel `data_satus`
--
ALTER TABLE `data_satus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `team_fk_6963767` (`team_id`);

--
-- Indeks untuk tabel `data_satu_input_data`
--
ALTER TABLE `data_satu_input_data`
  ADD KEY `input_data_id_fk_6963798` (`input_data_id`),
  ADD KEY `data_satu_id_fk_6963798` (`data_satu_id`);

--
-- Indeks untuk tabel `data_tigas`
--
ALTER TABLE `data_tigas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `team_fk_6963779` (`team_id`);

--
-- Indeks untuk tabel `data_tiga_input_data`
--
ALTER TABLE `data_tiga_input_data`
  ADD KEY `input_data_id_fk_6963800` (`input_data_id`),
  ADD KEY `data_tiga_id_fk_6963800` (`data_tiga_id`);

--
-- Indeks untuk tabel `input_datas`
--
ALTER TABLE `input_datas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `team_fk_6963805` (`team_id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `permission_role`
--
ALTER TABLE `permission_role`
  ADD KEY `role_id_fk_6963741` (`role_id`),
  ADD KEY `permission_id_fk_6963741` (`permission_id`);

--
-- Indeks untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `role_user`
--
ALTER TABLE `role_user`
  ADD KEY `user_id_fk_6963750` (`user_id`),
  ADD KEY `role_id_fk_6963750` (`role_id`);

--
-- Indeks untuk tabel `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`),
  ADD KEY `owner_fk_6963760` (`owner_id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `team_fk_6963761` (`team_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `data_duas`
--
ALTER TABLE `data_duas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `data_empats`
--
ALTER TABLE `data_empats`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `data_satus`
--
ALTER TABLE `data_satus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `data_tigas`
--
ALTER TABLE `data_tigas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `input_datas`
--
ALTER TABLE `input_datas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT untuk tabel `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `teams`
--
ALTER TABLE `teams`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `data_duas`
--
ALTER TABLE `data_duas`
  ADD CONSTRAINT `team_fk_6963773` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`);

--
-- Ketidakleluasaan untuk tabel `data_dua_input_data`
--
ALTER TABLE `data_dua_input_data`
  ADD CONSTRAINT `data_dua_id_fk_6963799` FOREIGN KEY (`data_dua_id`) REFERENCES `data_duas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `input_data_id_fk_6963799` FOREIGN KEY (`input_data_id`) REFERENCES `input_datas` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `data_empats`
--
ALTER TABLE `data_empats`
  ADD CONSTRAINT `team_fk_6963785` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`);

--
-- Ketidakleluasaan untuk tabel `data_empat_input_data`
--
ALTER TABLE `data_empat_input_data`
  ADD CONSTRAINT `data_empat_id_fk_6963801` FOREIGN KEY (`data_empat_id`) REFERENCES `data_empats` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `input_data_id_fk_6963801` FOREIGN KEY (`input_data_id`) REFERENCES `input_datas` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `data_satus`
--
ALTER TABLE `data_satus`
  ADD CONSTRAINT `team_fk_6963767` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`);

--
-- Ketidakleluasaan untuk tabel `data_satu_input_data`
--
ALTER TABLE `data_satu_input_data`
  ADD CONSTRAINT `data_satu_id_fk_6963798` FOREIGN KEY (`data_satu_id`) REFERENCES `data_satus` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `input_data_id_fk_6963798` FOREIGN KEY (`input_data_id`) REFERENCES `input_datas` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `data_tigas`
--
ALTER TABLE `data_tigas`
  ADD CONSTRAINT `team_fk_6963779` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`);

--
-- Ketidakleluasaan untuk tabel `data_tiga_input_data`
--
ALTER TABLE `data_tiga_input_data`
  ADD CONSTRAINT `data_tiga_id_fk_6963800` FOREIGN KEY (`data_tiga_id`) REFERENCES `data_tigas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `input_data_id_fk_6963800` FOREIGN KEY (`input_data_id`) REFERENCES `input_datas` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `input_datas`
--
ALTER TABLE `input_datas`
  ADD CONSTRAINT `team_fk_6963805` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`);

--
-- Ketidakleluasaan untuk tabel `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_id_fk_6963741` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_id_fk_6963741` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_id_fk_6963750` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_id_fk_6963750` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `teams`
--
ALTER TABLE `teams`
  ADD CONSTRAINT `owner_fk_6963760` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `team_fk_6963761` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
